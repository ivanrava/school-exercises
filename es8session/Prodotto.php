<?php

class Prodotto
{
    private $id;
    private $nome;
    private $genere;
    private $prezzo;
    private $giacenza;
    private $immagine;

    /**
     * Prodotto constructor.
     */
    function Prodotto() {
        $this->id       = null;
        $this->nome     = null;
        $this->genere   = null;
        $this->prezzo   = null;
        $this->giacenza = null;
        $this->immagine = null;
    }

    /**
     * Inizializza gli attributi dell'oggetto
     *
     * @param $nome
     * @param $genere
     * @param $prezzo
     * @param $giacenza
     * @param $immagine
     */
    function init($id, $nome, $genere, $prezzo, $giacenza, $immagine) {
        $this->id = $id;
        $this->nome = $nome;
        $this->genere = $genere;
        $this->prezzo = $prezzo;
        $this->giacenza = $giacenza;
        $this->immagine = $immagine;
    }

    /**
     * Inizializza la classe a partire da 1 stringa CSV
     * con il seguente formato:
     *  nome;genere;prezzo;giacenza;immagine
     *
     * @param $csv_string
     */
    public function setCSV($csv_string, $delimiter=";") {
        $fields = explode($delimiter, $csv_string);
        $this->init($fields[0], $fields[1], $fields[2], $fields[3], $fields[4], $fields[5]);
    }

    /**
     * Ritorna 1 stringa CSV con il seguente formato:
     *  nome;genere;prezzo;giacenza;immagine
     *
     * @return string
     */
    public function getCSV($delimiter=";") {
        return implode($delimiter, [
            $this->id,
            $this->nome,
            $this->genere,
            $this->prezzo,
            $this->giacenza,
            $this->immagine
        ]);
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return null
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param null $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return null
     */
    public function getGenere()
    {
        return $this->genere;
    }

    /**
     * @param null $genere
     */
    public function setGenere($genere)
    {
        $this->genere = $genere;
    }

    /**
     * @return null
     */
    public function getPrezzo()
    {
        return $this->prezzo;
    }

    /**
     * @param null $prezzo
     */
    public function setPrezzo($prezzo)
    {
        $this->prezzo = $prezzo;
    }

    /**
     * @return null
     */
    public function getGiacenza()
    {
        return $this->giacenza;
    }

    /**
     * @param null $giacenza
     */
    public function setGiacenza($giacenza)
    {
        $this->giacenza = $giacenza;
    }

    /**
     * @return null
     */
    public function getImmagine()
    {
        return $this->immagine;
    }

    /**
     * @param null $immagine
     */
    public function setImmagine($immagine)
    {
        $this->immagine = $immagine;
    }
}