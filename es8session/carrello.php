<?php
/*
 * Se l'utente non è loggato, ritorna alla home.
 */
session_start();
if (!isset($_SESSION["nome"]) && !isset($_SESSION["cognome"])) {
    header("Location: index.php");
    die();
} else {
    require_once "config.php";
    require_once "Prodotto.php";
    require_once "Cart.php";
    $prodotti = array();
    foreach (file(CSV_PRODOTTI) as $line) {
        $tmp = new Prodotto();
        $tmp->setCSV($line);
        $prodotti[$tmp->getId()] = $tmp;
    }

    // Se sono stati passati i dati per un acquisto...
    if (isset($_POST["add"]) && isset($_POST["id"]) && isset($_POST["qt"])) {
        // ...Controlla se esiste il prodotto con l'id passato...
        if (array_key_exists($_POST["id"], $prodotti)) {
            // ...Prendi il prodotto
            $prodotto = $prodotti[$_POST["id"]];
            // Controlla se esiste una quantità accettabile
            if ($_POST["qt"] <= $prodotto->getGiacenza() && $_POST["qt"] > 0) {
                $cart = isset($_COOKIE["carrello"]) ? new Cart($_COOKIE["carrello"]) : new Cart();
                $cart->add($prodotto, $_POST["qt"]);
                $cookie = $cart->to_csv();
                setcookie("carrello", $cookie);
            } else {
                // quantità illegale
                $error = "Quantità illegale";
            }
        } else {
            // id non trovato
            $error = "ID non trovato";
        }
    } else if (isset($_POST["remove"]) && isset($_POST["id"]) && isset($_POST["qt"])) {
        // ...Controlla se esiste il prodotto con l'id passato...
        if (array_key_exists($_POST["id"], $prodotti)) {
            // ...Prendi il prodotto dall'array
            $prodotto = $prodotti[$_POST["id"]];

            $cart = isset($_COOKIE["carrello"]) ? new Cart($_COOKIE["carrello"]) : new Cart();
            $status = $cart->remove($prodotto, $_POST["qt"]);

            $cookie = $cart->to_csv();
            setcookie("carrello", $cookie);
        } else {
            // id non trovato
            $error = "ID non trovato";
        }
    }
}
?>
<html>
<head>
    <title>Carrello</title>
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<nav>
    <a href="index.php" class="button" role="button">Back</a>
    <?php if(isset($cookie) || (isset($_COOKIE["carrello"]) && !isset($_POST["remove"]))): ?>
        <a href="conferma.php" class="button-secondary" role="button">Conferma l'ordine</a>
    <?php endif; ?>
</nav>
<main>
    <?php if(isset($error)): ?>
        <h2><?=$error?></h2>
    <?php endif; ?>
    <?php if(isset($cookie) || (isset($_COOKIE["carrello"]) && !isset($_POST["remove"]))): ?>
        <!--        Esiste il carrello -->
        <h1>Il tuo carrello</h1>
        <table>
            <thead>
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">Immagine</th>
                <th scope="col">Quantità</th>
                <th scope="col">Prezzo</th>
                <th scope="col">Totale</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $totale = 0; // Variabile accumulatore
            // Inizializza il carrello, con il cookie (dal web o dal codice precedente)
            $cart = isset($cookie) ? new Cart($cookie) : new Cart($_COOKIE["carrello"]);
            $articoli = $cart->getCart();
            ?>

            <?php foreach ($articoli as $id=>$qt): ?>
                <tr>
                    <?php
                    $prodotto = $prodotti[$id];
                    ?>
                    <td><?=$prodotto->getNome();?></td>
                    <td>
                        <img src="<?=$prodotto->getImmagine()?>">
                    </td>
                    <td><?=$qt?></td>
                    <td><?=$prodotto->getPrezzo()?> €</td>
                    <td>
                        <?php
                        $tmp = $prodotto->getPrezzo()*$qt;
                        $totale+=$tmp;
                        echo $tmp;
                        ?> €
                    </td>
                    <td>
                        <form method="POST">
                            <form method="POST">
                                <input name="id" type="hidden" value="<?=$prodotto->getId()?>" required="required">
                                <input name="qt" type="hidden" value="<?=$qt?>" required="required">
                                <button type="submit" name="remove">Rimuovi dal carrello</button>
                            </form>
                        </form>
                    </td>
                </tr>
            <?php endforeach; ?>
            <tr>
                <td colspan="4" scope="row">TOTALE ORDINE</td>
                <td><?=$tmp?> €</td>
            </tr>
            </tbody>
        </table>
    <?php else: ?>
        <!--        Non esiste il carrello -->
        <h1>Il tuo carrello è vuoto</h1>
    <?php endif; ?>
</main>
</body>
</html>