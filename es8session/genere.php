<?php
/*
 * Se è passato / impostato il genere
 *      Itera sul file, per ogni riga CSV crea un'istanza
 *           Se genere == genere passato
 *                  Stampa in tabella
 * Altrimenti
 *      Stampa la form di immissione del genere
 */
session_start();
?>

<html>
<head>
    <title>ProdAzienda</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<nav>
    <?php if(isset($_SESSION["nome"]) && isset($_SESSION["cognome"])): ?>
        <p>
            Sei loggato come <b><?=$_SESSION["nome"]." ".$_SESSION["cognome"]?></b>
            <a href="index.php" class="button" role="button">Back</a>
            <a href="carrello.php" class="button-secondary" role="button">Carrello</a>
        </p>
    <?php else: ?>
        <a href="login.php" class="button" role="button">Login</a>
    <?php endif; ?>
</nav>
<main>
    <?php if(isset($_POST["genere"])): ?>
        <?php
        require_once "config.php";
        require_once "Prodotto.php";
        $prodotti = array();
        foreach (file(CSV_PRODOTTI) as $line) {
            $prodotto = new Prodotto();
            $prodotto->setCSV($line);
            if ($prodotto->getGenere() == $_POST["genere"]) {
                array_push($prodotti, $prodotto);
            }
        }
        ?>
        <h1>Prodotti di genere <?=$_POST["genere"]?></h1>
        <table>
            <thead>
            <tr>
                <th scope="col">Prodotto</th><th scope="col">Prezzo</th><th scope="col">Giacenza</th><th scope="col">Immagine</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($prodotti as $prodotto): ?>
                <tr>
                    <td><?=$prodotto->getNome()?></td>
                    <td><?=$prodotto->getPrezzo()?> €</td>
                    <td><?=$prodotto->getGiacenza()?></td>
                    <?php if(isset($_SESSION["nome"]) && isset($_SESSION["cognome"])): ?>
                        <td>
                            <form action="carrello.php" method="POST">
                                <label for="qt">N°</label>
                                <input name="id" type="hidden" value="<?=$prodotto->getId()?>" required="required">
                                <input id="qt" name="qt" type="number" min="0" max="<?=$prodotto->getGiacenza()?>" required="required">
                                <?php if($prodotto->getGiacenza() == 0): ?>
                                    <button type="submit" name="add" disabled>Aggiungi al carrello</button>
                                <?php else: ?>
                                    <button type="submit" name="add">Aggiungi al carrello</button>
                                <?php endif; ?>
                            </form>
                        </td>
                    <?php endif; ?>
                    <td>
                        <img src="<?=$prodotto->getImmagine()?>">
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <?php require_once "config.php" ?>
        <form method="POST">
            <label for="genere">Genere</label>
            <select id="genere" name="genere" required="required">
                <?php foreach(read_generi(CSV_PRODOTTI) as $genere): ?>
                    <option value="<?=$genere?>"><?=$genere?></option>
                <?php endforeach; ?>
            </select>
            <button type="submit">Cerca prodotti</button>
        </form>
    <?php endif; ?>
</main>
</body>
</html>
