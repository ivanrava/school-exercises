<?php require_once "config.php" ?>
<?php require_once "Studente.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Mostra utenti</title>
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <?php include "_navbar.html" ?>
    <main>
        <?php if(file_exists(STUDENTI_CSV)): ?>
            <?php
            $studenti = array();
            foreach (file(STUDENTI_CSV) as $line) {
                $studenti[explode(";", $line)[0]] = new Studente($line);
            }
            ksort($studenti);
            ?>
            <h1><?=count($studenti)?> studenti iscritti</h1>
            <table>
                <thead>
                    <tr>
                        <th>Nome</th><th>Cognome</th><th>Scuola</th><th>Indirizzo</th><th>Nascita</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($studenti as $studente): ?>
                        <tr>
                            <td><?=$studente->getNome()?></td>
                            <td><?=$studente->getCognome()?></td>
                            <td><?=$studente->getScuola()?></td>
                            <td><?=$studente->getIndirizzo()?></td>
                            <td><?=$studente->getNascita()?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php else: ?>
            <h1>Il file non esiste!</h1>
        <?php endif; ?>
    </main>
</body>
</html>