<?php

class Studente
{
    private $nome;
    private $cognome;
    private $scuola;
    private $indirizzo;
    private $nascita;

    /**
     * Studente constructor. La stringa CSV riceve i
     * parametri nel formato:
     *      nome;cognome;scuola;indirizzo;datanascita
     * @param $csv
     */
    function __construct($csv)
    {
        $attr = explode(";", $csv);
        $this->setNome($attr[0]);
        $this->setCognome($attr[1]);
        $this->setScuola($attr[2]);
        $this->setIndirizzo($attr[3]);
        $this->setNascita($attr[4]);
    }

    /**
     * Ritorna gli attributi dell'oggetto come .csv
     *
     * @return string
     */
    function export_csv()
    {
        return implode(";", [
            $this->getNome(),
            $this->getCognome(),
            $this->getScuola(),
            $this->getIndirizzo(),
            $this->getNascita()
        ]);
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getCognome()
    {
        return $this->cognome;
    }

    /**
     * @param mixed $cognome
     */
    public function setCognome($cognome)
    {
        $this->cognome = $cognome;
    }

    /**
     * @return mixed
     */
    public function getScuola()
    {
        return $this->scuola;
    }

    /**
     * @param mixed $scuola
     */
    public function setScuola($scuola)
    {
        $this->scuola = $scuola;
    }

    /**
     * @return mixed
     */
    public function getIndirizzo()
    {
        return $this->indirizzo;
    }

    /**
     * @param mixed $indirizzo
     */
    public function setIndirizzo($indirizzo)
    {
        $this->indirizzo = $indirizzo;
    }

    /**
     * @return mixed
     */
    public function getNascita()
    {
        return $this->nascita;
    }

    /**
     * @param mixed $nascita
     */
    public function setNascita($nascita)
    {
        $this->nascita = $nascita;
    }
}