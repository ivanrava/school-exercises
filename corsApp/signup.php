<?php require_once 'settings.php'; ?>
<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>CorsApp - Registra utente</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <?php if (!empty($_POST)): ?>
            <!-- Controlla se è stato confermato tutto -->
            <?php if (isset($_POST["confirm"]) && isset($_POST["username"]) && isset($_POST["corso"])): ?>
                <!-- Scrivi i dati -->
                <?php if (!$file = fopen(FILENAME_UTENTI, "a")): ?>
                    <h2>Errore del server in creazione, contatta l'amministratore!</h2>
                <!-- Struttura file: utente;corso -->
                <!-- Sui campi viene effettuata una server-side validation,
                dai quali vengono "stripped" i caratteri invalidi -->
                <!-- In caso di errore, feedback per l'utente -->
                <?php elseif (fwrite($file, ssv($_POST["username"]).";".ssv($_POST["corso"])."\n") === FALSE): ?>
                    <h2>Errore del server in scrittura, contatta l'amministratore!</h2>
                <!-- Feedback di successo -->
                <?php else: ?>
                    <h2>Utente registrato con successo</h2>
                    <a href="index.html">Torna alla dashboard</a>
                <?php endif; ?>
            <!-- Controlla se sono stati passati i dati per richiedere conferma -->
            <?php elseif (isset($_POST["username"]) && isset($_POST["corso"])): ?>
                <!-- Conferma -->
                <h1>Conferma i dati</h1>
                <form method="post">
                    <?php
                    // Prendi i dati dagli array
                    $docente = $corsiDocenti[$_POST["corso"]];
                    $costo = $docentiCosti[$docente];
                    ?>
                    <!-- Metti i dati passati come readonly, per feedback -->
                    <input type="text" name="username" value="<?php echo $_POST["username"]; ?>" readonly>
                    <input type="text" name="corso" value="<?php echo $_POST["corso"]; ?>" readonly>
                    <p>
                        Docente: <?php echo $docente; ?>
                    </p>
                    <p>
                        Costo: <?php echo $costo; ?> €
                    </p>
                    <button type="submit" name="confirm">Conferma la registrazione!</button>
                </form>
            <?php else: ?>
                <!-- Feedback di errore -->
                <h1>Dati non inviati correttamente al server!</h1>
            <?php endif; ?>
        <?php else: ?>
            <!-- Mostra la form di registrazione -->
            <h1>Registra un nuovo utente</h1>
            <form method="post">
                <input type="text" name="username" required>
                <?php require '_selectCorsi.php'; ?>
                <button type="submit" name="signup">Registrati!</button>
            </form>
        <?php endif; ?>
    </body>
</html>
