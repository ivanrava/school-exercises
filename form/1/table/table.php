<?php
// Richiedi (una volta) le funzioni definite nel file esterno
require_once 'functions.php';

$min = $_GET['min'];
$max = $_GET['max'];
?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Fattoriale <?php echo $min."-".$max; ?></title>
        <link type="text/css" rel="stylesheet" href="main.css">
    </head>
    <body>
        <?php if ($min<=0 || $max<=0 || $max < $min || $max == "" || $min == "" || $min > 100 || $max > 100): ?>
            <h1>Errore</h1>
            <p>
                <b>I dati inviati al server non sono validi!</b> Per favore, riprova a
                <a href="../index.html">compilare la form</a>.
            </p>
        <?php else: ?>
            <table>
                <tr>
                    <th scope="col">N</th><th scope="col">Fattoriale</th>
                </tr>
                <?php
                // Itera tra i numeri passati in GET (compresi) calcolando e mostrando il fattoriale a lato
                for ($i=$min; $i <= $max; $i++) {
                    echo "<tr scope='row'>
                    <td>$i</td>
                    <td>".fact($i)."</td>
                    </tr>";
                }
                ?>
            </table>
        <?php endif; ?>
    </body>
</html>
