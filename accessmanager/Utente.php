<?php

require_once "config.php";
require_once "Accesso.php";

class Utente
{
    private $id;
    private $username;
    private $password;
    private $nome;
    private $cognome;
    private $nascita;
    private $email;
    private $indirizzo;
    private $tipo;

    public function login($username, $password)
    {
        // Effettua la connessione
        $link = new mysqli(host, username, password, db);
        if (!$link) {
            return false;
        }
        // Esegui la query
        $result = $link->query(
            'SELECT * FROM utenti WHERE username = "'.$username.'" AND password = "'.$password.'"'
        );
        // Se trova, true e idrata
        if ($result->num_rows == 1) {
            $row =  $result->fetch_assoc();
            $this->setUsername($row["username"]);
            $this->setPassword($row["password"]);
            $this->setId($row["id"]);
            $this->setNome($row["nome"]);
            $this->setCognome($row["cognome"]);
            $this->setEmail($row["email"]);
            $this->setIndirizzo($row["indirizzo"]);
            $this->setNascita($row["nascita"]);
            $this->setTipo($row["tipo"]);

            $accesso = new Accesso();
            $accesso->setUserId($this->getId());
            $accesso->setLogin(date("Y-m-d H:i:s"));
            return $accesso->save();
            // altrimenti, false
        } else return false;
    }

    public function store() {
        $link = new mysqli(host, username, password, db);
        if (!$link) {
            return false;
        }
        if (
            is_null($this->getUsername()) ||
            is_null($this->getPassword()) ||
            is_null($this->getNome()) ||
            is_null($this->getCognome()) ||
            is_null($this->getEmail()) ||
            is_null($this->getIndirizzo()) ||
            is_null($this->getNascita())
        ) {
            return false;
        } else {
            $result = $link->query("INSERT INTO utenti(username, tipo, password, nome, cognome, email, indirizzo, nascita)
            VALUES ('".$this->getUsername()."', ".$this->getTipo().", '".$this->getPassword()."', '".$this->getNome()."', '".$this->getCognome()."',
            '".$this->getEmail()."', '".$this->getIndirizzo()."', '".$this->getNascita()."')");
            return $result;
        }
    }

    /**
     * Aggiorna l'utente all $id specificato nel DB
     *
     * @param $id
     * @return bool|mysqli_result
     */
    public function update($id)
    {
        // Effettua la connessione
        $link = new mysqli(host, username, password, db);
        if (!$link) {
            return false;
        }
        // Esegui la query: cerca l'utente che si vuole aggiornare
        $result = $link->query("SELECT * FROM utenti WHERE id = $id");
        if($result->num_rows == 1) {
            // Imposta l'id della classe
            $this->setId($result->fetch_assoc()["id"]);
            // Aggiorna l'utente a questo id e controlla l'esito
            $result = $link->query("UPDATE utenti SET
                username='".$this->getUsername()."', password='".$this->getPassword()."', nome='".$this->getNome()."', cognome='".$this->getCognome()."', 
                email='".$this->getEmail()."', indirizzo='".$this->getIndirizzo()."', nascita='".$this->getNascita()."'
                WHERE id=".$this->getId());
            return $result;
        } else return false;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getCognome()
    {
        return $this->cognome;
    }

    /**
     * @param mixed $cognome
     */
    public function setCognome($cognome)
    {
        $this->cognome = $cognome;
    }

    /**
     * @return mixed
     */
    public function getNascita()
    {
        return $this->nascita;
    }

    /**
     * @param mixed $nascita
     */
    public function setNascita($nascita)
    {
        $this->nascita = $nascita;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getIndirizzo()
    {
        return $this->indirizzo;
    }

    /**
     * @param mixed $indirizzo
     */
    public function setIndirizzo($indirizzo)
    {
        $this->indirizzo = $indirizzo;
    }

    /**
     * @return mixed
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}