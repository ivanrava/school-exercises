<?php
// Apri la sessione
session_start();
// Variabile per il feedback dell'errore
$query_error = null;
// Controlla se l'utente è loggato
if(isset($_SESSION["tipo"])) {
    // Controlla se l'utente può visualizzare questa pagina:
    //  - l'utente è amministrativo
    //  - lo username è passato
    if(isset($_GET["username"]) && $_SESSION["tipo"]) {
        // Crea la connessione al DB
        require_once "config.php";
        $link = new mysqli(host, username, password, db);
        if (!$link) {
            // Error feedback
            $query_error = "Errore nella creazione del collegamento";
        } else {
            // Prendi gli accessi dell'utente con username X
            $result = $link->query("SELECT * FROM accessi INNER JOIN utenti ON accessi.userId = utenti.id WHERE username = '".$_GET["username"]."'");
            if ($result->num_rows == 0) {
                // Error feedback
                $query_error = "Non è stato trovato nessun accesso per ".$_GET['username'];
            }
        }
        // Altrimenti redirect
    } else header("Location: index.php");
    // Altrimenti redirect
} else header("Location: index.php");
?>
<html>
    <head>
        <title>Elenco accessi per <?=$_GET["username"]?></title>
    </head>
    <body>
        <?php if(isset($query_error)): ?>
            <h1><?=$query_error?></h1>
        <?php else: ?>
            <h1>Elenco accessi per <?=$_GET["username"]?></h1>
            <table>
                <tr>
                    <th>Login</th><th>Logout</th>
                </tr>
                <?php while($row = $result->fetch_assoc()): ?>
                    <tr>
                        <td><?=$row["login"]?></td><td><?=$row["logout"]?></td>
                    </tr>
                <?php endwhile; ?>
            </table>
        <?php endif; ?>
    </body>
</html>