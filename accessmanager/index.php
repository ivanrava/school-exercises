<?php
session_start();
if(!isset($_SESSION["username"])) {
    header("Location: login.php");
}
if (isset($_POST["logout"])) {
    session_destroy();
    require_once "Accesso.php";
    $accesso = new Accesso();
    $accesso->lastLogin($_SESSION["id"]);
    $accesso->setLogout(date("Y-m-d H:i:s"));
    $logout_status = $accesso->logout();

    if($logout_status) {
        header("Location: login.php?logout=Logout effettuato con successo");
    } else {
        header("Location: login.php?logout=Errore: non è stato possibile memorizzare la sessione");
    }
}
$update_status = null;
if (isset($_POST["update"])) {
    require_once "Utente.php";
    $utente = new Utente();
    // Idrata un utente
    $utente->setUsername($_POST["username"]);
    $utente->setPassword($_POST["password"]);
    $utente->setNome($_POST["nome"]);
    $utente->setCognome($_POST["cognome"]);
    $utente->setEmail($_POST["email"]);
    $utente->setNascita($_POST["nascita"]);
    $utente->setIndirizzo($_POST["indirizzo"]);
    // Aggiorna all'id passato
    $update_status = $utente->update($_SESSION["id"]);
    // Se va tutto bene, aggiorna la sessione
    if($update_status) {
        $_SESSION["username"] = $_POST["username"];
        $_SESSION["password"] = $_POST["password"];
        $_SESSION["nome"] = $_POST["nome"];
        $_SESSION["cognome"] = $_POST["cognome"];
        $_SESSION["email"] = $_POST["email"];
        $_SESSION["nascita"] = $_POST["nascita"];
        $_SESSION["indirizzo"] = $_POST["indirizzo"];
    }
}
$adduser_status = null;
if (isset($_POST["adduser"])) {
    require_once "Utente.php";
    $utente = new Utente();
    $utente->setUsername($_POST["username"]);
    $utente->setPassword($_POST["password"]);
    $utente->setNome($_POST["nome"]);
    $utente->setCognome($_POST["cognome"]);
    $utente->setEmail($_POST["email"]);
    $utente->setIndirizzo($_POST["indirizzo"]);
    $utente->setNascita($_POST["nascita"]);
    $utente->setTipo($_POST["tipo"]);
    $adduser_status = $utente->store();
}
?>
<html>
<head>
    <title>SQLAPP - Homepage</title>
</head>
<body>
<main>
    <?php if($adduser_status != null): ?>
        <?php if ($adduser_status): ?>
            <p>Utente aggiunto con successo!</p>
        <?php else: ?>
            <p>Errore nell'aggiunta dell'utente</p>
        <?php endif; ?>
    <?php endif; ?>
    <?php if($update_status != null): ?>
        <?php if ($update_status): ?>
            <p>Utente aggiornato con successo!</p>
        <?php else: ?>
            <p>Errore nell'aggiornamento dei dati dell'utente</p>
        <?php endif; ?>
    <?php endif; ?>
    <h1>Area personale</h1>
    <hr>
    <h3>Modifica i tuoi dati</h3>
    <p>Aggiorna i dati personali come preferisci. Quando hai finito premi <b>conferma</b>!</p>
    <form method="post">
        <fieldset>
            <legend>Dati per l'accesso</legend>
            <label for="username">Username</label>
            <input id="username" name="username" value="<?=$_SESSION["username"]?>" required>
            <label for="password">Password</label>
            <input id="password" name="password" type="password" value="<?=$_SESSION["password"]?>" required>
        </fieldset>
        <fieldset>
            <legend>Dati personali</legend>
            <label for="nome">Nome</label>
            <input id="nome" name="nome" value="<?=$_SESSION["nome"]?>" required>
            <label for="cognome">Cognome</label>
            <input id="cognome" name="cognome" value="<?=$_SESSION["cognome"]?>" required>
            <label for="email">Indirizzo e-mail</label>
            <input id="email" name="email" value="<?=$_SESSION["email"]?>" required>
            <label for="nascita">Data di nascita</label>
            <input id="nascita" name="nascita" type="date" value="<?=$_SESSION["nascita"]?>" required>
            <label for="indirizzo">Indirizzo</label>
            <input id="indirizzo" name="indirizzo" value="<?=$_SESSION["indirizzo"]?>" required>
        </fieldset>

        <button type="submit" name="update">Aggiorna i dati</button>
    </form>
    <hr>
    <?php if($_SESSION["tipo"]): ?>
    <h3>Aggiungi un nuovo utente</h3>
    <form method="post">
        <fieldset>
            <legend>Dati per l'accesso</legend>
            <label for="username">Username</label>
            <input id="username" name="username" required>
            <label for="password">Password</label>
            <input id="password" name="password" type="password" required>
            <label for="tipo">Tipo / privilegi dell'utente</label>
            <select id="tipo" name="tipo">
                <option value="0">Utente normale</option>
                <option value="1">Amministratore</option>
            </select>
        </fieldset>
        <fieldset>
            <legend>Dati personali</legend>
            <label for="nome">Nome</label>
            <input id="nome" name="nome" required>
            <label for="cognome">Cognome</label>
            <input id="cognome" name="cognome" required>
            <label for="email">Indirizzo e-mail</label>
            <input id="email" name="email" required>
            <label for="nascita">Data di nascita</label>
            <input id="nascita" name="nascita" type="date" required>
            <label for="indirizzo">Indirizzo</label>
            <input id="indirizzo" name="indirizzo" required>
        </fieldset>

        <button type="submit" name="adduser">Aggiungi utente</button>
    </form>
    <hr>
    <h3>Query</h3>
    <h5>Elenco accessi di un utente</h5>
    <form action="elenco_accessi.php" method="get">
        <input name="username" placeholder="Inserisci uno username...">
        <button type="submit">Vai!</button>
    </form>
    <h5>Elenco accessi compresi tra 2 date</h5>
    <form name="query2" action="elenco_accessi_date.php" method="get">
        <fieldset>
            <legend>Data di partenza dell'intervallo</legend>

            <label for="d1">Giorno</label>
            <select id="d1" name="d1"></select>

            <label for="m1">Mese</label>
            <select id="m1" name="m1">
                <option value="1">Gennaio</option>
                <option value="2">Febbraio</option>
                <option value="3">Marzo</option>
                <option value="4">Aprile</option>
                <option value="5">Maggio</option>
                <option value="6">Giugno</option>
                <option value="7">Luglio</option>
                <option value="8">Agosto</option>
                <option value="9">Settembre</option>
                <option value="10">Ottobre</option>
                <option value="11">Novembre</option>
                <option value="12">Dicembre</option>
            </select>

            <label for="y1">Anno</label>
            <select id="y1" name="y1" class="year-select"></select>
        </fieldset>
        <fieldset>
            <legend>Data di fine dell'intervallo</legend>

            <label for="d2">Giorno</label>
            <select id="d2" name="d2"></select>

            <label for="m2">Mese</label>
            <select id="m2" name="m2">
                <option value="1">Gennaio</option>
                <option value="2">Febbraio</option>
                <option value="3">Marzo</option>
                <option value="4">Aprile</option>
                <option value="5">Maggio</option>
                <option value="6">Giugno</option>
                <option value="7">Luglio</option>
                <option value="8">Agosto</option>
                <option value="9">Settembre</option>
                <option value="10">Ottobre</option>
                <option value="11">Novembre</option>
                <option value="12">Dicembre</option>
            </select>

            <label for="y2">Anno</label>
            <select id="y2" name="y2" class="year-select"></select>
        </fieldset>
        <button type="submit">Invia!</button>
    </form>
    <h5>Elenco alfabetico utenti con più di 10 accessi in un mese</h5>
    <form action="elenco_utenti.php" method="get">
        <label for="mese">Scegli il mese</label>
        <select id="mese" name="mese">
            <option value="1">Gennaio</option>
            <option value="2">Febbraio</option>
            <option value="3">Marzo</option>
            <option value="4">Aprile</option>
            <option value="5">Maggio</option>
            <option value="6">Giugno</option>
            <option value="7">Luglio</option>
            <option value="8">Agosto</option>
            <option value="9">Settembre</option>
            <option value="10">Ottobre</option>
            <option value="11">Novembre</option>
            <option value="12">Dicembre</option>
        </select>
        <button type="submit">Invia!</button>
    </form>
    <hr>
    <?php endif; ?>
    <h3>Logout</h3>
    <form method="post">
        <label for="logout">Effettua il logout</label>
        <button id="logout" name="logout" type="submit">Esci</button>
    </form>
</main>
<script src="index.ir"></script>
</body>
</html>
