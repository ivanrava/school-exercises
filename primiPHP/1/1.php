<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>
            Tabella
            <?php
            // Mostra la data secondo la stringa di formato (Ora:Minuti Giorno Mese(stringa) Anno)
            echo date("H:i j M Y");
            ?></title>
        <link type="text/css" rel="stylesheet" href="../main.css">
    </head>
    <body>
        <table>
            <tr>
                <th scope="col">N</th><th scope="col">N^2</th><th scope="col">N^3</th>
            </tr>
            <?php
            for ($i=1; $i <= 15; $i++) {
                // Itera tra 1 e 15 (compresi) e mostra la riga di tabella con
                // le potenze (seconda e terza) dei numeri
                echo "<tr scope='row'>
                            <td>$i</td>
                            <td>".pow($i, 2)."</td>
                            <td>".pow($i, 3)."</td>
                        </tr>";
            }
            ?>
        </table>
    </body>
</html>
